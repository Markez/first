from django.shortcuts import render
import logging

logging = logging.getLogger('test_logger')
# Create your views here.


def testpage(request):
    if request.method == 'GET':
        return render(request, 'testhome.html')
    elif request.method == 'POST':
        logging.info('Ready to process post data')