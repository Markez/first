from django.views.decorators.cache import cache_page
from django.urls import path, re_path

from . import views

urlpatterns = [
    # urls in cache list
    path('v2/test/', views.testpage, name='testpage'),
    path('v3/test/', views.testpage, name='testpage'),

    # url that is not in cache list
    path('v1/test/', views.testpage, name='testpage'),

]
