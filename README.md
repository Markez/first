# Cache Middleware

- Variable in settings file which defines URLS to cache and the time to cache them, e.g. CACHE_URLS = [('^api/v1/test', 60*60)]
- Middleware which checks the request URL  and caches the page for the duration specified in settings. If the page was fou

First install python and create an environment from where you will install all the packages required to run the application including django framework.
### Prerequisites

* Django==2.1.5
* django-decouple==2.1
* django-redis==4.10.0
* python-memcached==1.59
* pytz==2018.9
* redis==3.1.0
* six==1.12.0

We have a single page that when accessed using the urls that are in the CACHE_URLs list, the content will be cached
And when we edit the page contents and try accessing the same url we will still get the same contents that are in the cache for as long as the duration has not expired yet
But when accessed using url that is not in the CACHE_URLs list we will get changed contents every time we make a change

## URLS in the CACHE_URLS
* http://127.0.0.1:8000/api/v2/test/
* http://127.0.0.1:8000/api/v3/test/

## URLS that aren't in the CACHE_URLS
* http://127.0.0.1:8000/api/v1/test/


